﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealPad : MonoBehaviour
{
    public float healAmount;

    public GameObject explosionPrefab;

    public Sound sound;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        HealPlayer(collision);
    }


    private void HealPlayer(Collider2D collision)
    {
        //Debug.Log("col " + collision.name);

        if (collision.GetComponent<Player>())
        {
            Health health = collision.GetComponent<Health>();
            health.Heal(healAmount);

            Instantiate(explosionPrefab, this.transform.position, Quaternion.identity, TempObjectsPool.POOL);

            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sound, "heal");

            Destroy(this.gameObject);
        }
    }
}
