﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField]
    LayerMask lmWalls;
    [SerializeField]
    float jumpVelocity = 5;

    Rigidbody2D rigid;

    float jumpPressedRemember = 0;
    [SerializeField]
    float jumpPressedRememberTime = 0.2f;

    float groundedRemember = 0;
    [SerializeField]
    float groundedRememberTime = 0.25f;

    [SerializeField]
    float speed = 1;

    [SerializeField]
    [Range(0, 1)]
    float cutJumpHeight = 0.5f;

    public bool hasDoubleJump = false;

    public Sound jumpSound;


    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        bool bGrounded = IsGrounded();

        groundedRemember -= Time.deltaTime;
        if (bGrounded)
        {
            hasDoubleJump = true;
            groundedRemember = groundedRememberTime;
        }

        jumpPressedRemember -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpPressedRemember = jumpPressedRememberTime;
        }

        if (Input.GetKeyUp(KeyCode.Space) && hasDoubleJump)
        {
            if (rigid.velocity.y > 0)
            {
                rigid.velocity = new Vector2(rigid.velocity.x, rigid.velocity.y * cutJumpHeight);
            }
        }

        if ((jumpPressedRemember > 0) && (groundedRemember > 0))
        {
            jumpPressedRemember = 0;
            groundedRemember = 0;
            rigid.velocity = new Vector2(rigid.velocity.x, jumpVelocity);
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(jumpSound, "jump");
        }
        else if (hasDoubleJump && Input.GetKeyDown(KeyCode.Space))
        {
            rigid.velocity = new Vector2(rigid.velocity.x, jumpVelocity);
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(jumpSound, "jump");
            hasDoubleJump = false;
        }

        //float fHorizontalVelocity = Input.GetAxisRaw("Horizontal") * speed;

        //rigid.velocity = new Vector2(fHorizontalVelocity, rigid.velocity.y);

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            float x = Input.GetAxisRaw("Horizontal") * speed;
            rigid.AddForce((x - rigid.velocity.x) * Vector2.right * 2);
        }
        else
        {
            rigid.AddForce(-rigid.velocity.x * Vector2.right * 4f);

        }




    }

    private bool IsGrounded()
    {
        bool bGrounded = Physics2D.OverlapBox((Vectors.TransformToVector2(transform) + v2GroundedBoxCheckPosition), v2GroundedBoxCheckScale, 0, lmWalls);
        return bGrounded;
    }

    [SerializeField]
    private Vector2 v2GroundedBoxCheckPosition;
    [SerializeField]
    private Vector2 v2GroundedBoxCheckScale;

    private void OnDrawGizmosSelected()
    {
        if (IsGrounded())
        {
            Gizmos.color = Color.green;
        }
        else
        {
            Gizmos.color = Color.red;
        }

        Gizmos.DrawWireCube(Vectors.TransformToVector2(transform) + v2GroundedBoxCheckPosition, v2GroundedBoxCheckScale);
    }
}
