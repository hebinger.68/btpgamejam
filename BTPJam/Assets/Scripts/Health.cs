﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour, IDamageable
{
    public float health;
    public float maxHealth;

    [Range(0, 10)]
    public float noDamageDuration;
    private float lastDamageTime;

    public List<DamageSource> damageSources;

    public void Damage(float amount, DamageSource source)
    {


        if (!damageSources.Contains(source))
        {
            return;
        }

        if (lastDamageTime + noDamageDuration > Time.time)
        {
            return;
        }

        lastDamageTime = Time.time;
        health -= amount;

        OnDamage.Invoke();

        if (health <= 0)
        {
            health = 0;

            OnDeath.Invoke();
        }
    }

    public void Heal(float amount)
    {

        health += amount;

        if (health > maxHealth)
        {
            health = maxHealth;
        }

        OnHeal.Invoke();
    }

    public UnityEvent OnDamage;
    public UnityEvent OnHeal;
    public UnityEvent OnDeath;
}
