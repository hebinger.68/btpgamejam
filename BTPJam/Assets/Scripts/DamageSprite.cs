﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSprite : MonoBehaviour
{
    public Sprite [] sprites;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Health health;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites [(sprites.Length - 1)];
    }

    public void OnHit()
    {
        int breakStage = 0;

        breakStage = (int)((health.health / health.maxHealth) * (sprites.Length - 1));


        spriteRenderer.sprite = sprites [Mathf.Clamp(breakStage, 0, sprites.Length - 1)];
    }
}
