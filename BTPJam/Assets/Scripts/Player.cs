﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public GameObject damagePrefab;
    public Sound damageSound;

    public float shakeIntensity = 1;
    public float shakeDuration = 0.1f;

    private Rigidbody2D rigid;

    public HealthBarWithIndicator healthBar;

    private Health health;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        health = GetComponent<Health>();

        UpdateHealthBar();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDamage()
    {
        Instantiate(damagePrefab, transform.position, Quaternion.identity, TempObjectsPool.POOL);
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(damageSound, "player damage");
        CinemachineShake.Instance.ShakeCamera(shakeIntensity, shakeDuration);

        rigid.velocity = rigid.velocity * 0.5f;
        UpdateHealthBar();
    }

    public void OnHeal()
    {
        UpdateHealthBar();
    }

    public void OnDeath()
    {
        Instantiate(damagePrefab, transform.position, Quaternion.identity, TempObjectsPool.POOL);
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(damageSound, "player damage");
        CinemachineShake.Instance.ShakeCamera(shakeIntensity * 4, shakeDuration * 5);
        this.gameObject.SetActive(false);
    }

    public void UpdateHealthBar()
    {
        healthBar.SetValue(health.health / ((health.maxHealth != 0) ? health.maxHealth : health.health));
    }
}
