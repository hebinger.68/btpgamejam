﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    private Health health;
    public Gun gun;

    public float gunFireDelay;
    public float gunFireDelayUp;

    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
    }

    public void OnHealthChange()
    {

        float newFireDelay = health.health.Remap(0, health.maxHealth, gunFireDelayUp, gunFireDelay);
        gun.fireDelay = newFireDelay;

    }
}
