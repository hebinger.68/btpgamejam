﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer door;
    private SpriteRenderer spriteRenderer;


    public Sprite [] sprites = new Sprite [2];

    public GameObject light;

    public bool isOpen;

    private float openTime;
    [SerializeField]
    private float openDuration;
    private float doorStartPosition;


    public float shakeIntensity;

    public Sound sound;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites [0];
        light.SetActive(false);
        openTime = openDuration;
        doorStartPosition = door.transform.position.y;
    }

    public void OnDoorTrigger()
    {
        if (isOpen) return;
        isOpen = true;

        spriteRenderer.sprite = sprites [1];
        light.SetActive(true);

        CinemachineShake.Instance.ShakeCamera(shakeIntensity, openDuration / 2);

        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sound, "door");

    }

    private void Update()
    {
        if (isOpen && openTime > 0)
        {
            openTime -= Time.deltaTime;

            float doorSize = door.size.x;

            float y = Mathf.Lerp(0, doorSize, 1 - (openTime / openDuration));

            door.transform.position = new Vector3(door.transform.position.x, doorStartPosition + y, 0);

            light.SetActive(true);


        }
    }
}
