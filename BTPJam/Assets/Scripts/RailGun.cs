using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailGun : MonoBehaviour
{

    private new Camera camera;

    public float fireDelay;
    private float fireCooldown = 0;

    [SerializeField]
    private Sound [] sounds;

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 position = Vectors.TransformToVector2(transform);

        Vector2 cursor = camera.ScreenToWorldPoint(Input.mousePosition);

        float angle = Vector2.SignedAngle(Vector2.right, cursor - position);

        Quaternion rotation = Quaternion.Euler(0, 0, angle);



        fireCooldown -= Time.deltaTime;


        if (Input.GetMouseButton(0) && fireCooldown <= 0)
        {
            fireCooldown = fireDelay;

            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sounds [Random.Range(0, sounds.Length)], "sootsound");
        }
    }
}

