﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarWithIndicator : MonoBehaviour
{
    [SerializeField]
    private Slider barFill;
    [SerializeField]
    private Slider barFillBack;

    private float damageTimer;

    [SerializeField]
    private float backFillLatency;

    private float latencyStartValue;

    public Color flashColor;

    private Color fillColor;
    private Image fillImage;


    private void Start()
    {
        fillImage = barFill.fillRect.GetComponent<Image>();
        fillColor = fillImage.color;
    }

    // Start is called before the first frame update
    public void SetValue(float value)
    {
        damageTimer = backFillLatency;
        latencyStartValue = barFill.value;

        barFill.value = value;
    }

    private void Update()
    {
        if (damageTimer > 0)
        {

            if (damageTimer > backFillLatency * 0.66f)
            {
                fillImage.color = flashColor;
            }
            else
            {
                fillImage.color = fillColor;
            }

            damageTimer -= Time.deltaTime;

            barFillBack.value = Mathf.Lerp(latencyStartValue, barFill.value, 1 - (damageTimer / backFillLatency));

        }
    }
}
