﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Start is called before the first frame update

    public static AudioManager AUDIO_MANAGER;

    private void Awake()
    {
        AUDIO_MANAGER = this;
    }

    public void PlayAudioGlobal(Sound s, string name)
    {
        CreateAudioObject(s, name);
    }

    private void CreateAudioObject(Sound s, string name)
    {

        GameObject t = new GameObject("sound:" + name);
        t.transform.SetParent(Camera.main.transform);
        AudioPlayer player = t.AddComponent<AudioPlayer>();
        player.PlaySound(s);

    }

    private void CutAudio(string name)
    {
        Destroy(GameObject.Find("sound:" + name));
    }


}
