﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{

    public TextMeshProUGUI textMesh;

    public double timeSeconds;

    public string format = "hh':'mm':'ss':'fff";

    public bool isCounnting;

    // Start is called before the first frame update
    void Start()
    {
        textMesh.text = FloatToTimer(timeSeconds, format);
    }

    // Update is called once per frame
    void Update()
    {
        if (isCounnting)
        {
            timeSeconds += Time.deltaTime;

        }
        textMesh.text = FloatToTimerAlt(timeSeconds);

    }

    public string FloatToTimer(double seconds, string format)
    {
        TimeSpan time = TimeSpan.FromSeconds(seconds);
        return time.ToString(format);
    }


    public string FloatToTimerAlt(double seconds)
    {
        TimeSpan time = TimeSpan.FromSeconds(seconds);

        string output = string.Format("{0}:{1}:{2}",
         // Display the total minutes, throwing away the fractional portion.
         (int)time.TotalMinutes,
         // Display the seconds component (0-59) as two digits with a leading
         // zero if necessary.
         time.Seconds.ToString("D2"),

         time.Milliseconds.ToString("D3")

         );

        return output;
    }


}
