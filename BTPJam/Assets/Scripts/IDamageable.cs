﻿public interface IDamageable
{
    void Damage(float amount, DamageSource source);
    void Heal(float amount);
}

public enum DamageSource
{
    Enemy,
    Player,
    Ambient
}
