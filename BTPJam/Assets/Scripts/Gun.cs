﻿using UnityEngine;

public class Gun : MonoBehaviour
{

    private new Camera camera;

    public GameObject bulletPrefab;

    public Transform bulletParent;

    public float fireDelay;
    private float fireCooldown = 0;

    [SerializeField]
    private Sound [] sounds;

    public float shakeIntensity = 1;
    public float shakeDuration = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 position = Vectors.TransformToVector2(transform);

        Vector2 cursor = camera.ScreenToWorldPoint(Input.mousePosition);

        float angle = Vector2.SignedAngle(Vector2.right, cursor - position);

        Quaternion rotation = Quaternion.Euler(0, 0, angle);

        this.transform.rotation = rotation;



        fireCooldown -= Time.deltaTime;


        if (Input.GetMouseButton(0) && fireCooldown <= 0)
        {
            GameObject bullet = Instantiate(bulletPrefab, this.transform.position, rotation, bulletParent);
            bullet.GetComponent<Bullet>().Launch((cursor - position).normalized);
            fireCooldown = fireDelay;

            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sounds [Random.Range(0, sounds.Length)], "sootsound");

            CinemachineShake.Instance.ShakeCamera(shakeIntensity, shakeDuration);
        }
    }
}
