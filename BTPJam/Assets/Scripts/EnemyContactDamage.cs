﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContactDamage : MonoBehaviour
{

    public float damage;
    public DamageSource source = DamageSource.Enemy;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        HurtPlayer(collision);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        HurtPlayer(collision);
    }


    private void HurtPlayer(Collider2D collision)
    {
        //Debug.Log("col " + collision.name);

        if (collision.GetComponent<Player>())
        {
            Health health = collision.GetComponent<Health>();
            health.Damage(damage, source);
        }
    }
}
