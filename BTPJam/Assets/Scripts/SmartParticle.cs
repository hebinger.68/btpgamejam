﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartParticle : MonoBehaviour
{

    public Transform target;

    // Start is called before the first frame update
    void Start()
    {
        system = GetComponent<ParticleSystem>();

        if (system == null)
        {
            Debug.LogWarning("Particle system not found !! " + this.name);
            Destroy(this);
        }
    }

    private ParticleSystem system;

    // Update is called once per frame
    void Update()
    {
        if (!system.IsAlive(true))
        {
            Destroy(this.gameObject);
        }

        if (target != null)
        {
            this.transform.position = target.position;
        }
    }
}
