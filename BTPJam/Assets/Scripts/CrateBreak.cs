﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateBreak : MonoBehaviour
{

    public Sprite [] sprites = new Sprite [5];
    public int breakStage = 0;

    private SpriteRenderer spriteRenderer;

    public GameObject drop;

    private void Start()
    {
        if (breakStage >= sprites.Length)
        {
            Debug.LogError("break stage to high " + this.name);
            breakStage = 0;
        }

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites [breakStage];
    }

    public void OnHit()
    {
        if (breakStage < sprites.Length - 1)
        {
            breakStage++;
            spriteRenderer.sprite = sprites [breakStage];
        }
        else
        {
            Break();
        }
    }

    private void Break()
    {
        if (drop != null)
            Instantiate(drop, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}