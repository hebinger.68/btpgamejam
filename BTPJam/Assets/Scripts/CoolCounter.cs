﻿using UnityEngine;

public class CoolCounter : MonoBehaviour
{

    private float value;
    [Range(0.1f, 10)]
    public float duration = 1;

    private float interpolateAmount;

    public TMPro.TextMeshPro textMesh;

    public string format;

    // Update is called once per frame
    void Update() {

        interpolateAmount = interpolateAmount + (Time.deltaTime / duration);
        if (interpolateAmount <= 1) {
            interpolateAmount = 1;
        }

        float inter = Mathf.Lerp(0, value, interpolateAmount);

        textMesh.text = string.Format(format, inter);
    }

    public void SetValue(float value) {
        interpolateAmount = 0;
        this.value = value;
    }
}
