﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed = 5;

    public LayerMask Solid;

    private new Collider2D collider;

    public GameObject explosion;

    public float damageRadius = 0.2f;
    public float damage = 5;
    public DamageSource damageSource;

    public float lifeTime = 5;

    private void Start()
    {
        collider = this.GetComponent<Collider2D>();
    }

    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Explode();
        }
    }

    public void Launch(Vector2 vector)
    {
        GetComponent<Rigidbody2D>().velocity = vector * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collider != null && collider.IsTouchingLayers(Solid))
        {
            Explode();
        }
    }

    private void Explode()
    {
        Instantiate(explosion, this.transform.position, Quaternion.identity, TempObjectsPool.POOL);

        Collider2D [] hits = Physics2D.OverlapCircleAll(this.transform.position, damageRadius, Solid);

        foreach (Collider2D hit in hits)
        {
            Health health = hit.GetComponent<Health>();
            if (health != null)
            {
                health.Damage(damage, damageSource);
            }
        }

        Destroy(this.gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, damageRadius);
    }
}
