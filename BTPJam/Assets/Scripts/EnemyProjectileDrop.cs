﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileDrop : MonoBehaviour
{
    public float detectionRange;
    public GameObject bulletPrefab;

    public float fireDelay;
    private float fireCooldown;
    private Transform player;

    void Start()
    {
        player = FindObjectOfType<Player>().transform;
        if (player == null)
        {
            Debug.LogError("Did not find Player " + this.name);
        }
    }


    void FixedUpdate()
    {
        if (fireCooldown > 0 || player == null)
        {
            fireCooldown -= Time.deltaTime;
            return;
        }


        if (Vector2.Distance(this.transform.position, player.transform.position) <= detectionRange)
        {
            fireCooldown = fireDelay;

            GameObject bullet = Instantiate(bulletPrefab, this.transform.position, Quaternion.identity, TempObjectsPool.POOL);
            //bullet.GetComponent<Bullet>().Launch((player.transform.position - this.transform.position).normalized);
            bullet.GetComponent<Rigidbody2D>().angularVelocity = 100;
            bullet.GetComponent<Rigidbody2D>().gravityScale = 1;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, detectionRange);
    }
}
