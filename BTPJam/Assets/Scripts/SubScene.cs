﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubScene : MonoBehaviour
{
    [SerializeField]
    private GameObject subScene;

    [SerializeField]
    private Vector2 activatePosition1;
    [SerializeField]
    private Vector2 activatePosition2;

    private Player player;

    public float updateTime;
    private float updateCooldwon;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (player == null) return;

        updateCooldwon -= Time.deltaTime;
        if (updateCooldwon <= 0)
        {
            updateCooldwon += updateTime;
        }
        else
        {
            return;
        }


        subScene.SetActive(IsPlayerInside());


    }



    public bool IsPlayerInside()
    {
        Vector2 playerPosition = player.transform.position;

        return ((playerPosition.x > activatePosition1.x && playerPosition.x < activatePosition2.x &&
            playerPosition.y > activatePosition1.y && playerPosition.y < activatePosition2.y));
    }


    private void OnDrawGizmosSelected()
    {
        player = FindObjectOfType<Player>();
        if (player == null) return;

        Vector2 size = new Vector2(((activatePosition2.x - activatePosition1.x)),
            ((activatePosition2.y - activatePosition1.y)));

        Gizmos.color = (IsPlayerInside()) ? Color.green : Color.red;


        Gizmos.DrawRay(activatePosition1, Vector2.up * size.y);
        Gizmos.DrawRay(activatePosition1, Vector2.right * size.x);
        Gizmos.DrawRay(activatePosition2, Vector2.down * size.y);
        Gizmos.DrawRay(activatePosition2, Vector2.left * size.x);

        Gizmos.DrawLine(activatePosition1, activatePosition2);
        Gizmos.DrawSphere(activatePosition1, 0.1f);
    }

}
