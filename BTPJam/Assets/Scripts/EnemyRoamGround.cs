﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRoamGround : MonoBehaviour
{
    private Rigidbody2D rigid;

    public LayerMask Solid;

    public Vector2 pointRightWall;
    public Vector2 pointLeftWall;
    public float height;
    public float depth;

    public float speed;
    public float force;

    Vector2 direction = Vector2.right;

    private Vector2 pointRightWallWorld
    {
        get {
            return new Vector2(transform.position.x + pointRightWall.x, transform.position.y + pointRightWall.y);

        }
    }
    private Vector2 pointLeftWallWorld
    {
        get {
            return new Vector2(transform.position.x + pointLeftWall.x, transform.position.y + pointLeftWall.y);

        }
    }


    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (direction.x > 0)
        {
            if (isWall(pointRightWallWorld) || !isGround(pointRightWallWorld))
            {
                direction = Vector2.left;
            }
        }
        else
        {
            if (isWall(pointLeftWallWorld) || !isGround(pointLeftWallWorld))
            {
                direction = Vector2.right;
            }
        }


        rigid.AddForce((speed - rigid.velocity.magnitude) * direction.normalized * force);



    }

    bool isGround(Vector2 position)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, Vector2.down, depth, Solid);
        return hit.collider != null;
    }

    bool isWall(Vector2 position)
    {
        RaycastHit2D hit = Physics2D.Raycast(position, Vector2.up, height, Solid);
        return hit.collider != null;
    }

    private void OnDrawGizmosSelected()
    {
        if (isGround(pointLeftWallWorld))
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawRay(pointLeftWallWorld, Vector2.down * depth);

        if (isGround(pointRightWallWorld))
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawRay(pointRightWallWorld, Vector2.down * depth);


        if (isWall(pointLeftWallWorld))
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawRay(pointLeftWallWorld, Vector2.up * height);


        if (isWall(pointRightWallWorld))
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawRay(pointRightWallWorld, Vector2.up * height);
    }

}
