﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyFollow : MonoBehaviour
{
    public float detectionRange;

    private Transform player;
    private Rigidbody2D rigid;

    public float speed;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<Player>().transform;
        if (player == null)
        {
            Debug.LogError("Did not find Player " + this.name);
        }
    }


    void FixedUpdate()
    {


        if (Vector2.Distance(this.transform.position, player.transform.position) <= detectionRange)
        {
            Vector2 direction = (player.transform.position - this.transform.position).normalized;
            rigid.AddForce((speed - rigid.velocity.magnitude) * direction);
        }
        else
        {
            rigid.AddForce(-rigid.velocity.magnitude * rigid.velocity.normalized * 2f);

        }



    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, detectionRange);
    }
}
