﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public GameObject damagePrefab;
    public Sound damageSound;

    public GameObject deathPrefab;
    public Sound deathSound;

    // Start is called before the first frame update
    void Start()
    {
        if (deathPrefab == null || damagePrefab == null)
        {
            Debug.LogWarning("prefabs undefined ! " + this.name);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDeath()
    {
        if (deathPrefab != null)
            Instantiate(damagePrefab, transform.position, Quaternion.identity, TempObjectsPool.POOL);
        if (deathSound != null)
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(deathSound, "enemy damage");
        Destroy(this.gameObject);
    }

    public void OnDamage()
    {
        if (damagePrefab != null)
            Instantiate(deathPrefab, transform.position, Quaternion.identity, TempObjectsPool.POOL);
        if (damageSound != null)
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(damageSound, "enemy damage");
    }
}
