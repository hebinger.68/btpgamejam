﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crow : MonoBehaviour
{

    public float detectionRange;

    public Vector2 flightDirection;

    private Animator animator;

    private Transform player;
    private Rigidbody2D rigid;

    private bool detected;

    public float destroyRange;

    void Start()
    {
        player = FindObjectOfType<Player>().transform;
        if (player == null)
        {
            Debug.LogError("Did not find Player " + this.name);
        }
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        if (detected)
        {
            if (Vector2.Distance(this.transform.position, player.transform.position) >= destroyRange)
            {
                Destroy(this.gameObject);

            }
        }
        else
        {
            if (Vector2.Distance(this.transform.position, player.transform.position) <= detectionRange)
            {
                animator.enabled = true;
                detected = true;

                rigid.velocity = flightDirection;
            }
        }


    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(this.transform.position, detectionRange);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, destroyRange);
    }
}
