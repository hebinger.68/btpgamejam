﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

    public GameObject [] enemies;

    public GameObject explosion;

    private int enemyToSpawn = 0;

    public Sprite deathSprite;

    private Health health;

    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnHit()
    {
        if (health.health == 0) return;


        for (int i = 0; i < 8; i++)
        {
            Vector3 v = transform.position;
            v.x += -2.5f + i;
            v.y -= (2.5f + i) / 2;
            Instantiate(enemies [enemyToSpawn], v, Quaternion.identity);
        }


        enemyToSpawn += 1;
        if (enemyToSpawn >= enemies.Length)
        {
            enemyToSpawn = 0;
        }
    }

    public void OnDeath()
    {
        GetComponent<SpriteRenderer>().sprite = deathSprite;

        GameOverMenu.INSTANCE.onPlayerVictory();

        for (int i = 0; i < 7; i++)
        {
            Vector3 v = transform.position;
            v.x += -2 + i;
            v.y -= (2 + i) / 2;
            Instantiate(explosion, v, Quaternion.identity);
        }

        CinemachineShake.Instance.ShakeCamera(7, 2);
    }

}
