﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOverMenu : MonoBehaviour
{
    public Canvas deathCanvas;
    public Canvas highscoreCanvas;

    public Timer timer;

    public TextMeshProUGUI highscoreMesh;

    public static GameOverMenu INSTANCE;

    private void Start()
    {
        INSTANCE = this;
        Time.timeScale = 1;
    }

    public static void OnPlayerVictory()
    {
        INSTANCE.onPlayerVictory();
    }

    public void OnPlayerDeath()
    {
        timer.isCounnting = false;
        StartCoroutine(enableDeathCanvas());
    }

    public void onPlayerVictory()
    {
        timer.isCounnting = false;
        highscoreMesh.text = timer.FloatToTimerAlt(timer.timeSeconds);

        StartCoroutine(enableVictoryCanvas());
    }

    IEnumerator enableDeathCanvas()
    {
        yield return new WaitForSeconds(1f);

        deathCanvas.gameObject.SetActive(true);
        Time.timeScale = 0;

        yield return null;
    }

    IEnumerator enableVictoryCanvas()
    {
        yield return new WaitForSeconds(1f);

        highscoreCanvas.gameObject.SetActive(true);
        Time.timeScale = 0;

        yield return null;
    }
}
