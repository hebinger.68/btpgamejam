﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScrolling : MonoBehaviour
{
    // Start is called before the first frame update

    Material material;

    public Transform follow;

    public float offsetPerUnit;
    public float globalOffsetX;

    public int order;

    private void Start()
    {
        material = GetComponent<MeshRenderer>().material;

        GetComponent<MeshRenderer>().sortingLayerName = "BackGround";
        GetComponent<MeshRenderer>().sortingOrder = order;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(follow.position.x, transform.position.y, 0);

        float offsetX = follow.position.x * offsetPerUnit + globalOffsetX;

        Vector4 offset = material.GetVector("Offset_");
        offset.x = offsetX;

        material.SetVector("Offset_", offset);
    }
}
