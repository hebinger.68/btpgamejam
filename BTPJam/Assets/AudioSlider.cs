﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSlider : MonoBehaviour
{

    //public Mixer mixer;

    public Slider slider;
    public AudioMixer mixer;


    public void OnValueChanged()
    {
        mixer.SetFloat("mainVol", slider.value);
    }

}
